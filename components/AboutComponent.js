import React, { Component } from 'react';
import { Text, View, ScrollView, FlatList } from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = (state) => {
  return {
    leaders: state.leaders,
  };
};

function RenderHistory() {
  return (
    <Card title="Our History">
      <View style={{ marginLeft: 10 }}>
        <Text style={{ marginTop: 10 }}>
          Started in 2010, Ristorante con Fusion quickly established itself as a
          culinary icon par excellence in Hong Kong. With its unique brand of
          world fusion cuisine that can be found nowhere else, it enjoys
          patronage from the A-list clientele in Hong Kong. Featuring four of
          the best three-star Michelin chefs in the world, you never know what
          will arrive on your plate the next time you visit us.{' '}
        </Text>
        <Text style={{ marginTop: 10 }}>
          The restaurant traces its humble beginnings to The Frying Pan, a
          successful chain started by our CEO, Mr. Peter Pan, that featured for
          the first time the world's best cuisines in a pan.
        </Text>
      </View>
    </Card>
  );
}

class About extends Component {
  render() {
    const renderLeaders = ({ item, index }) => {
      return (
        <ListItem
          key={index}
          title={item.name}
          subtitle={item.description}
          hideChevron={true}
          leftAvatar={{ source: { uri: baseUrl + item.image } }}
        />
      );
    };

    if (this.props.leaders.isLoading) {
      return (
        <ScrollView>
          <View style={{ flex: 1 }}>
            <RenderHistory />
            <Card title="Corporate Leadership">
              <Loading />
            </Card>
          </View>
        </ScrollView>
      );
    } else if (this.props.leaders.errorMsg) {
      return (
        <ScrollView>
          <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
            <View style={{ flex: 1 }}>
              <RenderHistory />
              <Card title="Corporate Leadership">
                <Text>{this.props.leaders.errorMsg}</Text>
              </Card>
            </View>
          </Animatable.View>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView>
          <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
            <View style={{ flex: 1 }}>
              <RenderHistory />
              <Card title="Corporate Leadership">
                <FlatList
                  data={this.props.leaders.leaders}
                  renderItem={renderLeaders}
                  keyExtractor={(item) => item.id.toString()}
                />
              </Card>
            </View>
          </Animatable.View>
        </ScrollView>
      );
    }
  }
}

export default connect(mapStateToProps)(About);
