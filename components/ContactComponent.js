import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import * as MailComposer from 'expo-mail-composer';

function sendMail() {
  MailComposer.composeAsync({
    recipients: ['confusion@food.net'],
    subject: 'Enquiry',
    body: 'To whom it may concern:',
  });
}

const title = 'Contact information';
function Contact() {
  return (
    <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
      <Card title={title}>
        <View style={{ marginLeft: 10 }}>
          <Text style={{ marginTop: 10 }}>121, Clear Water Bay Road </Text>
          <Text style={{ marginTop: 10 }}>Clear Water Bay, Kowloon </Text>
          <Text style={{ marginTop: 10 }}>HONG KONG Tel: +852 1234 5678 </Text>
          <Text style={{ marginTop: 10 }}>Fax: +852 8765 4321 </Text>
          <Text style={{ marginTop: 10 }}>Email:confusion@food.net</Text>
          <Button
            title=" Send email"
            buttonStyle={{ backgroundColor: '#512da8' }}
            icon={
              <Icon name="envelope-o" type="font-awesome" color="#ffffff" />
            }
            onPress={() => sendMail()}
          />
        </View>
      </Card>
    </Animatable.View>
  );
}

export default Contact;
